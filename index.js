var mydb = {
    
};
var express = require("express");
var app = express();

var handlebars = require("express-handlebars");
app.engine("handlebars", handlebars({layout: false}));
app.set("view engine", "handlebars");

var session = require("express-session");

var config = {
    clientID: "__YOUR_CLIENT_ID__",
    clientSecret: "__YOUR_CLIENT_SECRET__",
    callbackURL: "http://__YOUR_CALLBACK__"
};

var googleapi = require("googleapis");
var OAuth2 = googleapi.auth.OAuth2;

var passport = require("passport");
var GoogleOAuth2 = require("passport-google-oauth").OAuth2Strategy;

var goauth2 = new GoogleOAuth2(config,
    function(accessToken, refreshToken, profile, done) {
        console.info("access: %s", accessToken)
        var profile = {
            email: profile.emails[0].value,
            photo: profile.photos[0].value,
            name: profile.displayName,
            accessToken: accessToken,
            refreshToken: refreshToken
        }
        mydb[profile.email] = profile;
        return (done(null, profile.email));
    }
);

passport.use(goauth2);

passport.serializeUser(function(user, done) {
    done(null, user);
});
passport.deserializeUser(function(id, done) {
    //Access to create user profile using id
    done(null, mydb[id]);
});

var login = require("connect-ensure-login");


//Process request
app.use(session({
    secret: "pukcats",
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());

app.use("/protected/*", login.ensureLoggedIn("/login/google"));

var scopes = [
    "https://www.googleapis.com/auth/drive",
    "email",
    "profile"
]

app.get("/login/google",
    passport.authenticate("google", {
        scope: scopes,
        accessType: "offline"
    })
);

app.get("/callback/google",
    passport.authenticate("google", {
        failureRedirect: "/error.html",
        successReturnToOrRedirect: "/protected/index"
    })
);

app.get("/protected/list", function(req, res) {

    var oauth2client = new OAuth2(config.clientID, config.clientSecret, config.callbackURL);
    console.info(">> access token: %s", req.user.accessToken);
    oauth2client.setCredentials({
        access_token: req.user.accessToken
    });
    var drive = googleapi.drive({version: "v3", auth: oauth2client});
    drive.files.list({"fields": ["files/name"]}, function(error, result, param3) {
        var result = arguments[2];
        //for (var i in arguments)
        console.info(">>> %s", JSON.stringify(error));
        console.info(">>> %s", JSON.stringify(result));
        console.info(">>> %s", JSON.stringify(param3));
        //console.info(">>> %s", JSON.stringify(result.body.files));
        var files = result.body.files;
        res.status(200);
        res.type("text/plain")
        res.send(files.map(function(f) { return (f.name + "\n")}));
    });
})

app.get("/protected/index", function(req, res) {
    res.render("index", {
        name: req.user.name,
        mypic: req.user.photo
    });
})


app.get("/logout", function(req, res) {
    req.logout();
    res.redirect("/index.html");
});

app.use(express.static(__dirname + "/public"));

app.listen(3000, function() {
    console.info("Application started on port 3000");
});




















